/*
   Author name:Rakesh Prajapati.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genie;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class sendMail {

   public static void main(String [] args) {     
      // Recipient's email ID needs to be mentioned.
   String to = "rp151090@gmail.com";

      // Sender's email ID needs to be mentioned
      String from = "Rakesh.Prajapati@ext.saint-gobain.com";

      // Assuming you are sending email from localhost
      String host = "smtp-a1.saint-gobain.com";

      // Get system properties
      Properties properties = System.getProperties();

      // Setup mail server
      properties.setProperty("mail.smtp.host", host);

      // Get the default Session object.
      Session session = Session.getDefaultInstance(properties);

      try {
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));

         // Set Subject: header field
         message.setSubject("Testing Mail By Rakesh");

         // Create the message part 
         BodyPart messageBodyPart = new MimeBodyPart();

         // Fill the message
//         messageBodyPart.setText("<b>Hi,<br/><b>"
//                 + "Please find the Attachment with the mail<br>"
//                 + "<b>Regards,<br/>"
//                 + "Rakesh Prajapati<b>");
         messageBodyPart.setText("Hi,Please find the Attachment with the mail-Regards,Rakesh Prajapati");
         
         // Create a multipar message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);

         // Part two is attachment
         messageBodyPart = new MimeBodyPart();
         String filename = "C:/xampp/htdocs/genie/src/genie/monthly.txt";
         DataSource source = new FileDataSource(filename);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(filename);
//         message.setContent(txtMg, "text/html; charset=utf-8");
         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart );

         // Send message
         Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
   }
}